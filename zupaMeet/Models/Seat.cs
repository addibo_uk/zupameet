﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ZupaMeet.Models
{
    // zupaMeet Venue Seats (linked to Venues)
    [DataContract]
    public class Seat
    {
        // Seat ID (auto-incrementing)
        [DataMember]
        public int ID { get; set; }

        // Seat number (in row)
        [Required]
        [DefaultValue(1)]
        [DataMember]
        public int Number { get; set; }

        // Seat Row (A, B, C etc.)
        [Required]
        [DefaultValue("A")]
        [DataMember]
        public string Row { get; set; }

        // Venue where the this seat is located
        [DataMember]
        public int VenueID { get; set; }
        public virtual Venue Venue { get; set; }

        // Seat is taken at an event
        [NotMapped]
        [DataMember]
        public bool? Booked { get; set; }

        // Hide Attendee if it's empty
        public bool ShouldSerializeBooked()
        {
            return Booked != null;
        }
    }
}
