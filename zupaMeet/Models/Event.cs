﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ZupaMeet.Models
{
    // zupaMeet Events
    [DataContract]
    public class Event
    {
        // Private variables
        DateTime? _StartDate;
        DateTime? _EndDate;

        // Event ID (auto-incrementing)
        [DataMember]
        public int ID { get; set; }

        // Event title
        [Required]
        [DataMember]
        public string Title { get; set; }

        // Optional event description
        [DataMember]
        public string Description { get; set; }

        // Event starting date/time (defaults to the start of today)
        [DataMember]
        public DateTime StartDate
        {
            get
            {
                return _StartDate.HasValue ? _StartDate.Value : DateTime.Today;
            }
            set
            {
                _StartDate = value;
            }
        }

        // Event ending date/time (defaults to start of tomorrow)
        [DataMember]
        public DateTime EndDate
        {
            get
            {
                return _EndDate.HasValue ? _EndDate.Value : DateTime.Today.AddDays(1);
            }
            set
            {
                _EndDate = value;
            }
        }

        // Maximum number of seats purchased in one booking (optional)
        [DataMember]
        public int? MaxSeatsPerBooking { get; set; }

        // Cost per seat for event
        [DataMember]
        [DefaultValue(0)]
        [DataType(DataType.Currency)]
        public float Cost { get; set; }

        // Venue ID of event (linked to Venues table)
        [DataMember]
        public int? VenueID { get; set; }

        // Venue object of event (linked to Venues table)
        [DataMember]
        public virtual Venue Venue { get; set; }

        // List of attendees at this event
        [NotMapped]
        [DataMember]
        public virtual List<Attendee> Attendees { get; set; }

        // Hide Venue if it's empty
        public bool ShouldSerializeVenue()
        {
            return Venue != null;
        }

        // Hide Attendees list if it's empty
        public bool ShouldSerializeAttendees()
        {
            return Attendees != null;
        }
    }
}
