﻿using Microsoft.EntityFrameworkCore;

namespace ZupaMeet.Models
{
    public class ZupaMeetContext : DbContext
    {
        // Database context object for zupaMeet
        public ZupaMeetContext(DbContextOptions<ZupaMeetContext> options) : base(options)
        {
        }

        // Objects representing tables in the database
        public DbSet<Event> Events { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Seat> Seats { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Attendee> Attendees { get; set; }
    }
}
