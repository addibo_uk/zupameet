﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ZupaMeet.Models
{
    // zupaMeet Booking Attemdees
    [DataContract]
    public class Attendee
    {
        // Attendee ID (auto-incrementing)
        [DataMember]
        public int ID { get; set; }

        // Name of person attending
        [Required]
        [DataMember]
        public string AttendeeName { get; set; }

        // Email address of person attending
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        [DataMember]
        public string Email { get; set; }

        // Seat ID for attendee (linked to Seats table)
        [DataMember]
        public int SeatID { get; set; }

        // Seat object for attendee (linked to Seats table)
        [DataMember]
        public virtual Seat Seat { get; set; }

        // Cost of seat
        [DataMember]
        [DataType(DataType.Currency)]
        public float Cost { get; set; }

        // Booking ID for attendee (linked to Bookings table)
        [DataMember]
        public int BookingID { get; set; }
        public virtual Booking Booking { get; set; }
    }
}
