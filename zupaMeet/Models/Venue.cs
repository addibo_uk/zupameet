﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ZupaMeet.Models
{
    // zupaMeet Event Venues (linked to Events)
    [DataContract]
    public class Venue
    {
        // Venue ID (auto-incrementing)
        [DataMember]
        public int ID { get; set; }

        // Venue title/name
        [Required]
        [DataMember]
        public string Title { get; set; }

        // Venue post code (for possible approximate Google Map location)
        [DataMember]
        public string PostCode { get; set; }

        // List of seats available at this Venue
        [DataMember]
        public virtual List<Seat> Seats { get; set; }

        // Hide Seat listing if it's empty
        public bool ShouldSerializeSeats() {  
            return Seats != null;  
        }
    }
}
