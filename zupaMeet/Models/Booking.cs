﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace ZupaMeet.Models
{
    // zupaMeet Bookings
    [DataContract]
    public class Booking
    {
        // Private variables
        DateTime? _BookingDate;

        // Booking ID (auto-incrementing)
        [DataMember]
        public int ID { get; set; }

        // Name of person making the booking
        [Required]
        [DataMember]
        public string BookingName { get; set; }

        // Email address of person making the booking
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address")]
        [DataMember]
        public string Email { get; set; }

        // Event starting date/time (defaults to the start of today)
        [DataMember]
        public DateTime BookingDate
        {
            get
            {
                return _BookingDate.HasValue ? _BookingDate.Value : DateTime.UtcNow;
            }
            set
            {
                _BookingDate = value;
            }
        }

        // Event ID of booking (linked to Events table)
        [DataMember]
        public int EventID { get; set; }

        // Event object of booking (linked to Events table)
        [DataMember]
        public virtual Event Event { get; set; }

        // Total cost of booking
        [DataType(DataType.Currency)]
        [DataMember]
        public float Cost { get; set; }

        // List of attendees in this booking
        [DataMember]
        public virtual List<Attendee> Attendees { get; set; }

        // Hide Event if it's empty
        public bool ShouldSerializeEvent()
        {
            return Event != null;
        }

        // Hide Attendee listing if it's empty
        public bool ShouldSerializeAttendees()
        {
            return Attendees != null;
        }
    }
}
