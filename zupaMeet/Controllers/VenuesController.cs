﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    public class VenuesController : Controller
    {
        readonly ZupaMeetContext _context;

        public VenuesController(ZupaMeetContext context)
        {
            _context = context;
        }

        // GET: Venues
        // List all Venues
        public async Task<List<Venue>> Index()
        {
            return await _context.Venues.ToListAsync();
        }

        // GET: Venues/5
        // Get a specific Venue and associated Seats
        [Route("Venues/{id:int}")]
        public async Task<IActionResult> Index(int id)
        {
            var venue = await _context.Venues.Include(v => v.Seats).SingleOrDefaultAsync(v => v.ID == id);
            if (venue != null)
            {
                // Return Venue
                return Ok(venue);
            }

            // No Venue found, return status 404 (Not Found)
            return NotFound();
        }

        // POST: Venues/Add
        // Add a new Venue to the system
        [HttpPost]
        [Route("Venues/Add")]
        public async Task<IActionResult> Add([FromBody] Venue venue)
        {
            // The ID is auto-generated
            venue.ID = 0;

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Make sure we aren't adding any seats
                venue.Seats = null;

                // Add Venue to the database
                _context.Add(venue);
                await _context.SaveChangesAsync();

                // Return status 200 (OK)
                return Ok(new { id = venue.ID });
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Venues/Edit/5
        // Edit an Venue in the system
        [HttpPost]
        [Route("Venues/Edit/{id:int}")]
        public async Task<IActionResult> Edit(int id, [FromBody] Venue venue)
        {
            // Check we're editing the correct Venue first
            if (id != venue.ID) 
            {
                // Return status 404 (Not Found)
                return NotFound();
            }

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                try
                {
                    // Make sure we aren't updating any seats
                    venue.Seats = null;

                    // Update Venue in the database
                    _context.Update(venue);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Maybe the Venue doesn't exist - return a status 400 (Bad Request) if so
                    if (!VenueExists(venue.ID))
                    {
                        return NotFound();
                    }
                    throw;
                }

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Venues/Delete/5
        // Delete a Venue from the system
        [HttpDelete]
        [Route("Venues/Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Check Venue with specified ID exists
            var venue = await _context.Venues.SingleOrDefaultAsync(v => v.ID == id);
            if (venue == null)
            {
                // No Venue found, return status 404 (Not Found)
                return NotFound();
            }

            // Remove Venue from database
            _context.Venues.Remove(venue);
            await _context.SaveChangesAsync();

            // Return status 200 (OK)
            return Ok();
        }

        // Check to see if a Venue exists in the system
        bool VenueExists(int id)
        {
            return _context.Venues.Any(v => v.ID == id);
        }
    }
}
