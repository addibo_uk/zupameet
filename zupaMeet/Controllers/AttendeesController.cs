﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    public class AttendeesController : Controller
    {
        readonly ZupaMeetContext _context;

        public AttendeesController(ZupaMeetContext context)
        {
            _context = context;
        }

        // GET: Attendees/5
        // Get a specific Attendee and associated Seat
        [Route("Attendees/{id:int}")]
        public async Task<IActionResult> Index(int id)
        {
            var attendee = await _context.Attendees.Include(a => a.Seat).SingleOrDefaultAsync(a => a.ID == id);
            if (attendee != null)
            {
                // Return Attendee
                return Ok(attendee);
            }

            // No Attendee found, return status 404 (Not Found)
            return NotFound();
        }

        // POST: Attendees/Edit/5
        // Edit an Attendee in a Booking
        [HttpPost]
        [Route("Attendees/Edit/{id:int}")]
        public async Task<IActionResult> Edit(int id, [FromBody] Attendee attendee)
        {
            // Get original Attendee for comparisons
            Attendee attendeeOrig = _context.Attendees.Include(a => a.Booking).AsNoTracking().SingleOrDefault(a => a.ID == id);

            // Check we're editing the correct Attendee first
            if (id != attendee.ID)
            {
                // Return status 404 (Not Found)
                return NotFound();
            }

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Make sure the Booking ID hasn't changed
                if (attendee.BookingID != attendeeOrig.BookingID)
                {
                    // Booking has changed, return status 400 (Bad Request)
                    return BadRequest(new { message = "Booking must not be changed" });
                }

                // Make sure the attendee cost is not changed
                attendee.Cost = attendeeOrig.Cost;

                // If we are moving seats, check the new seat is valid and is not occupied already
                if (attendee.SeatID != attendeeOrig.SeatID)
                {
                    // Check the new Seat is valid, first
                    var seat = _context.Seats.AsNoTracking().SingleOrDefault(s => s.ID == attendee.SeatID);
                    if (seat == null)
                    {
                        // No Seat found, return status 400 (Bad Request)
                        return BadRequest(new { message = "Invalid seat ID specified" });
                    }

                    if (_context.Attendees.Include(a => a.Booking).Any(a => a.SeatID == attendee.SeatID && a.Booking.EventID == attendeeOrig.Booking.EventID))
                    {
                        // Seat already taken in this Event, return status 400 (Bad Request)
                        return BadRequest(new { message = String.Format("Seat {0} on row {1} is already taken", seat.Number, seat.Row) });
                    }
                }

                try
                {
                    // Make sure we're not editing Seat data at the same time
                    attendee.Seat = null;

                    // Update Attendee in the database
                    _context.Update(attendee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Maybe the Attendee doesn't exist - return a status 400 (Bad Request) if so
                    if (!AttendeeExists(attendee.ID))
                    {
                        return NotFound();
                    }
                    throw;
                }

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Attendees/Delete/5
        // Delete an Attendee from a Booking
        [HttpDelete]
        [Route("Attendees/Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Check Attendee with specified ID exists
            var attendee = await _context.Attendees.SingleOrDefaultAsync(a => a.ID == id);
            if (attendee == null)
            {
                // No Attendee found, return status 404 (Not Found)
                return NotFound();
            }

            // Remove Attendee from database
            _context.Attendees.Remove(attendee);
            await _context.SaveChangesAsync();

            // Return status 200 (OK)
            return Ok();
        }

        // Check to see if an Attendee exists in the system
        bool AttendeeExists(int id)
        {
            return _context.Attendees.Any(a => a.ID == id);
        }
    }
}
