﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    public class SeatsController : Controller
    {
        readonly ZupaMeetContext _context;

        public SeatsController(ZupaMeetContext context)
        {
            _context = context;
        }

        // POST: Seats/Add
        // Add a new Seat to a Venue
        [HttpPost]
        [Route("Seats/Add")]
        public async Task<IActionResult> Add([FromBody] Seat seat)
        {
            // The ID is auto-generated
            seat.ID = 0;

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Check if Venue ID specified, is valid
                if (!_context.Venues.Any(v => v.ID == seat.VenueID))
                {
                    // No Venue found, return status 400 (Bad Request)
                    return BadRequest(new { row = seat.Row, number = seat.Number, message = "Invalid venue ID specified" });
                }

                // Check there isn't a seat already at the specified location
                if (_context.Seats.Any(s => s.Row == seat.Row && s.Number == seat.Number && s.VenueID == seat.VenueID))
                {
                    // Seat found, return status 400 (Bad Request)
                    return BadRequest(new { row = seat.Row, number = seat.Number, message = "Seat location already occupied" });
                }

                // Add Seat to the database
                _context.Add(seat);
                await _context.SaveChangesAsync();

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Seats/AddMultiple
        // Add a set of Seats to a Venue in one request
        [HttpPost]
        [Route("Seats/AddMultiple")]
        public async Task<IActionResult> AddMultiple([FromBody] List<Seat> seats)
        {
            // Make sure we have some seats to add
            if (seats != null && seats.Any())
            {
                // Loop through the seat list
                foreach (Seat seat in seats)
                {
                    // Call the "Add" method and catch the HTTP response
                    IActionResult result = await Add(seat);

                    // If the response isn't OK (status 200) stop and return result
                    var okObjectResult = result as OkResult;
                    if (okObjectResult == null)
                    {
                        return result;
                    }
                }

                // Return status 200 (OK)
                return Ok();
            }

            // No seats specified, return status 400 (Bad Request)
            return BadRequest(new { message = "No seats specified" });
        }

        // POST: Seats/Edit/5
        // Edit an Seat in a Venue
        [HttpPost]
        [Route("Seats/Edit/{id:int}")]
        public async Task<IActionResult> Edit(int id, [FromBody] Seat seat)
        {
            // Get original Seat for comparisons
            Seat seatOrig = _context.Seats.AsNoTracking().SingleOrDefault(s => s.ID == id);

            // Check we're editing the correct Seat first
            if (id != seat.ID)
            {
                // Return status 404 (Not Found)
                return NotFound();
            }

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Make sure the Venue ID hasn't changed
                if (seat.VenueID != seatOrig.VenueID)
                {
                    // Venue has changed, return status 400 (Bad Request)
                    return BadRequest(new { message = "Seat venue must not be changed" });
                }

                // Check there isn't a seat already at the specified location
                if (_context.Seats.Any(s => s.Row == seat.Row && s.Number == seat.Number && s.VenueID == seat.VenueID && s.ID != seat.ID))
                {
                    // Seat found, return status 400 (Bad Request)
                    return BadRequest(new { message = "Seat location already occupied" });
                }

                try
                {
                    // Update Seat in the database
                    _context.Update(seat);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Maybe the Seat doesn't exist - return a status 400 (Bad Request) if so
                    if (!SeatExists(seat.ID))
                    {
                        return NotFound();
                    }
                    throw;
                }

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Seats/Delete/5
        // Delete a Seat from the system
        [HttpDelete]
        [Route("Seats/Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Check Seat with specified ID exists
            var seat = await _context.Seats.SingleOrDefaultAsync(s => s.ID == id);
            if (seat == null)
            {
                // No Seat found, return status 404 (Not Found)
                return NotFound();
            }

            // Remove Seat from database
            _context.Seats.Remove(seat);
            await _context.SaveChangesAsync();

            // Return status 200 (OK)
            return Ok();
        }

        // Check to see if a Seat exists in the system
        bool SeatExists(int id)
        {
            return _context.Seats.Any(s => s.ID == id);
        }
    }
}
