﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    public class EventsController : Controller
    {
        readonly ZupaMeetContext _context;

        public EventsController(ZupaMeetContext context)
        {
            _context = context;
        }

        // GET: Events
        // List all Events and associated venues
        public async Task<List<Event>> Index()
        {
            return await _context.Events.Include(e => e.Venue).ToListAsync();
        }

        // GET: Events/5
        // Get a specific Event and associated Venue/Seats
        [Route("Events/{id:int}")]
        public async Task<IActionResult> Index(int id)
        {
            var @event = await _context.Events.Include(e => e.Venue).ThenInclude(v => v.Seats).SingleOrDefaultAsync(e => e.ID == id);
            if (@event != null)
            {
                // Get attendees' seats for this Event
                var seats = await _context.Attendees.Include(a => a.Booking).Where(a => a.Booking.EventID == @event.ID).Select(a => a.SeatID).ToListAsync();

                // Set "seat booked" state based on attendee list
                foreach (Seat seat in @event.Venue.Seats)
                {
                    seat.Booked = seats.Contains(seat.ID);
                }

                // Return Event
                return Ok(@event);
            }

            // No Event found, return status 404 (Not Found)
            return NotFound();
        }

        // GET: Events/Attendees/5
        // Get a specific Event and associated Venue/Attendees
        [Route("Events/Attendees/{id:int}")]
        public async Task<IActionResult> Attendees(int id)
        {
            var @event = await _context.Events.Include(e => e.Venue).SingleOrDefaultAsync(e => e.ID == id);
            if (@event != null)
            {
                // Get attendees for this Event
                var attendees = await _context.Attendees.Include(a => a.Booking).AsNoTracking().Include(a => a.Seat).Where(a => a.Booking.EventID == @event.ID).ToListAsync();

                // Add attendees to event object
                @event.Attendees = attendees;

                // Return Event
                return Ok(@event);
            }

            // No Event found, return status 404 (Not Found)
            return NotFound();
        }

        // POST: Events/Add
        // Add a new Event to the system
        [HttpPost]
        [Route("Events/Add")]
        public async Task<IActionResult> Add([FromBody] Event @event)
        {
            // The ID is auto-generated
            @event.ID = 0;

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Check if Venue ID specified, is valid
                if (@event.VenueID != null)
                {
                    if (!_context.Venues.Any(v => v.ID == @event.VenueID))
                    {
                        // No Venue found, return status 400 (Bad Request)
                        return BadRequest(new { message = "Invalid venue ID specified" });
                    }
                }

                // Make sure we're not adding a new Venue at the same time
                @event.Venue = null;

                // Add Event to the database
                _context.Add(@event);
                await _context.SaveChangesAsync();

                // Return status 200 (OK)
                return Ok(new { id = @event.ID });
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Events/Edit/5
        // Edit an Event in the system
        [HttpPost]
        [Route("Events/Edit/{id:int}")]
        public async Task<IActionResult> Edit(int id, [FromBody] Event @event)
        {
            // Get original Event for comparisons
            Event eventOrig = _context.Events.AsNoTracking().SingleOrDefault(e => e.ID == id);

            // Check we're editing the correct Event first
            if (id != @event.ID)
            {
                // Return status 404 (Not Found)
                return NotFound();
            }

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Make sure the Venue ID hasn't changed
                if (@event.VenueID != eventOrig.VenueID)
                {
                    // Event has changed, return status 400 (Bad Request)
                    return BadRequest(new { message = "Event venue must not be changed" });
                }

                try
                {
                    // Make sure we're not editing Venue data at the same time
                    @event.Venue = null;

                    // Update Event in the database
                    _context.Update(@event);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Maybe the Event doesn't exist - return a status 400 (Bad Request) if so
                    if (!EventExists(@event.ID))
                    {
                        return NotFound();
                    }
                    throw;
                }

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Events/Delete/5
        // Delete an Event from the system
        [HttpDelete]
        [Route("Events/Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Check Event with specified ID exists
            var @event = await _context.Events.SingleOrDefaultAsync(e => e.ID == id);
            if (@event == null)
            {
                // No Event found, return status 404 (Not Found)
                return NotFound();
            }

            // Remove Event from database
            _context.Events.Remove(@event);
            await _context.SaveChangesAsync();

            // Return status 200 (OK)
            return Ok();
        }

        // Check to see if an Event exists in the system
        bool EventExists(int id)
        {
            return _context.Events.Any(e => e.ID == id);
        }
    }
}
