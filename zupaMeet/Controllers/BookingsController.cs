﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    public class BookingsController : Controller
    {
        readonly ZupaMeetContext _context;

        public BookingsController(ZupaMeetContext context)
        {
            _context = context;
        }

        // GET: Bookings
        // List all Bookings and associated events
        public async Task<List<Booking>> Index()
        {
            return await _context.Bookings.Include(b => b.Event).ToListAsync();
        }

        // GET: Bookings/5
        // Get a specific Booking and associated Attendees
        [Route("Bookings/{id:int}")]
        public async Task<IActionResult> Index(int id)
        {
            var booking = await _context.Bookings.Include(b => b.Event).Include(b => b.Attendees).ThenInclude(a => a.Seat).SingleOrDefaultAsync(e => e.ID == id);
            if (booking != null)
            {
                // Return Booking
                return Ok(booking);
            }

            // No Booking found, return status 404 (Not Found)
            return NotFound();
        }

        // POST: Bookings/Add
        // Add a new Booking and associated Attendees to the system
        [HttpPost]
        [Route("Bookings/Add")]
        public async Task<IActionResult> Add([FromBody] Booking booking)
        {
            // The ID is auto-generated
            booking.ID = 0;

            // Do we have all the required/valid fields?
            if (ModelState.IsValid)
            {
                // Check first if we have any attendees
                if (booking.Attendees == null || (booking.Attendees != null && !booking.Attendees.Any()))
                {
                    // No attendees in this Booking, return status 400 (Bad Request)
                    return BadRequest(new { message = "There must be at least one attendee per booking" });
                }
               
                // Check the Event ID specified, is valid
                if (!_context.Events.Any(e => e.ID == booking.EventID))
                {
                    // No Event found, return status 400 (Bad Request)
                    return BadRequest(new { message = "Invalid event ID specified" });
                }

                // Check if the event has finished
                var @event = _context.Events.AsNoTracking().SingleOrDefault(e => e.ID == booking.EventID);
                if (@event.EndDate <= DateTime.Now)
                {
                    // The Event has finished, return status 400 (Bad Request)
                    return BadRequest(new { message = "The event specified has finished" });
                }

                // Check there are not too many attendees in this transaction
                if (booking.Attendees.Count() > @event.MaxSeatsPerBooking)
                {
                    // Too many attendees in this Booking, return status 400 (Bad Request)
                    return BadRequest(new { message = String.Format("There must only be {0} attendees per booking", @event.MaxSeatsPerBooking) });
                }

                // Check the requested Seats are not already taken
                float currentCost = 0;
                int attendeeCount = 1;
                foreach (Attendee attendee in booking.Attendees)
                {
                    // Check the Seat is valid, first
                    var seat = _context.Seats.AsNoTracking().SingleOrDefault(s => s.ID == attendee.SeatID);
                    if (seat == null)
                    {
                        // No Seat found, return status 400 (Bad Request)
                        return BadRequest(new { message = String.Format("Invalid seat ID specified for attendee {0}", attendeeCount) });
                    }

                    if (_context.Attendees.Include(a => a.Booking).Any(a => a.SeatID == attendee.SeatID && a.Booking.EventID == booking.EventID))
                    {
                        // Seat already taken in this Event, return status 400 (Bad Request)
                        return BadRequest(new { message = String.Format("Seat {0} on row {1} is already taken", seat.Number, seat.Row) });
                    }

                    // The IDs are all auto-generated/linked
                    attendee.ID = 0;
                    attendee.BookingID = 0;

                    // Make sure we're not editing Booking data at the same time
                    attendee.Booking = null;

                    // Make sure we're not editing Seat data at the same time
                    attendee.Seat = null;

                    // Save Attendee cost
                    attendee.Cost = @event.Cost;

                    // Add up the total cost of the booking as we go
                    currentCost += @event.Cost;
                    attendeeCount++;
                }

                // Set the cost of the Booking
                booking.Cost = currentCost;

                // Make sure we're not editing Event data at the same time
                booking.Event = null;

                // Process payment
                PaymentController payment = new PaymentController(booking);
                if (!payment.ProcessPayment())
                {
                    // Payment failed, return status 400 (Bad Request)
                    return BadRequest(new { message = "Booking payment failed" });
                }

                // Add Booking and Attendees to the database
                _context.Add(booking);
                await _context.SaveChangesAsync();

                // Return status 200 (OK)
                return Ok(new { id = booking.ID });
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Bookings/Edit/5
        // Edit a Booking (not its associated Attendees) in the system
        [HttpPost]
        [Route("Bookings/Edit/{id:int}")]
        public async Task<IActionResult> Edit(int id, [FromBody] Booking booking)
        {
            // Get original Booking for comparisons
            Booking bookingOrig = _context.Bookings.AsNoTracking().SingleOrDefault(b => b.ID == id);

            // Check we're editing the correct Booking first
            if (id != booking.ID)
            {
                // Return status 404 (Not Found)
                return NotFound();
            }

            // Check we have all the required/valid fields
            if (ModelState.IsValid)
            {
                // Make sure the booking date is not changed
                booking.BookingDate = bookingOrig.BookingDate;

                // Make sure the Event ID hasn't changed
                if (booking.EventID != bookingOrig.EventID)
                {
                    // Event has changed, return status 400 (Bad Request)
                    return BadRequest(new { message = "Booking event must not be changed" });
                }

                // Make sure the booking cost is not changed
                booking.Cost = bookingOrig.Cost;

                try
                {
                    // Take out any possible attendee data (we're not changing those here)
                    booking.Attendees = null;

                    // Make sure we're not editing Event data at the same time
                    booking.Event = null;

                    // Update Booking in the database
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    // Maybe the Booking doesn't exist - return a status 400 (Bad Request) if so
                    if (!BookingExists(booking.ID))
                    {
                        return NotFound();
                    }
                    throw;
                }

                // Return status 200 (OK)
                return Ok();
            }

            // Invalid/missing fields, return status 400 (Bad Request)
            return BadRequest(ModelState);
        }

        // POST: Bookings/Delete/5
        // Delete a Booking from the system
        [HttpDelete]
        [Route("Bookings/Delete/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            // Check Booking with specified ID exists
            var booking = await _context.Bookings.SingleOrDefaultAsync(b => b.ID == id);
            if (booking == null)
            {
                // No Booking found, return status 404 (Not Found)
                return NotFound();
            }

            // Remove Booking from database
            _context.Bookings.Remove(booking);
            await _context.SaveChangesAsync();

            // Return status 200 (OK)
            return Ok();
        }

        // Check to see if a Booking exists in the system
        bool BookingExists(int id)
        {
            return _context.Bookings.Any(b => b.ID == id);
        }
    }
}
