﻿using Microsoft.AspNetCore.Mvc;
using ZupaMeet.Models;

namespace ZupaMeet.Controllers
{
    // zupaMeet Payment Processing
    public class PaymentController : Controller
    {
        readonly Booking _booking;

        public PaymentController(Booking booking)
        {
            _booking = booking;
        }

        // Process the payment
        public bool ProcessPayment()
        {
            // Get the cost of the Booking
            var bookingCost = _booking.Cost;

            // TODO
            // Implement payment gateway stuff!

            // Return success for now
            return true;
        }
    }
}
