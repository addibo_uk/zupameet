﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ZupaMeet.Models;

namespace ZupaMeet.Extensions
{
    public static class DBExtensions
    {
        // Extemsion method for IWebHost to enable data seeding on launch
        public static IWebHost SeedData(this IWebHost host)
        {
            // Use dependency injection to get the DB context
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetService<ZupaMeetContext>();

                // Call data seeding method to add a default venue, seats and event
                AddVenueAndEvent(context);
            }
            return host;
        }

        // If there are no venues in the system, add a default one
        public static void AddVenueAndEvent(ZupaMeetContext context)
        {
            // Make sure we don't actually have a venue
            if (!context.Venues.Any())
            {
                // Create venue object and save to get insert ID
                var venue = new Venue() { Title = "zupaTech HQ", PostCode = "SO50 9DT" };
                context.Venues.Add(venue);
                context.SaveChanges();

                // Create 100 seats over 10 rows (A to J)
                var seats = new List<Seat>();
                for (char c = 'A'; c <= 'J'; c++)
                {
                    for (int n = 1; n <= 10; n++)
                    {
                        seats.Add(new Seat() { Row = c.ToString(), Number = n, VenueID = venue.ID });
                    }
                } 
            
                // Save seats to the database
                context.Seats.AddRange(seats);
                context.SaveChanges();

                // Create event object and save also
                var @event = new Event() { Title = "zupaMeetup " + DateTime.Now.Year.ToString(), Description = "Demo Event", StartDate = DateTime.Now.AddMonths(1), EndDate = DateTime.Now.AddMonths(1).AddDays(1), MaxSeatsPerBooking = 4, VenueID = venue.ID };
                context.Events.Add(@event);
                context.SaveChanges();
            }
        }

        // If there are no events in the system, add a default one
        public static void AddEvent(ZupaMeetContext context)
        {
            // Make sure we don't actually have an event
            if (!context.Events.Any())
            {
                // Create event object and save to get insert ID
                var venue = new Venue() { Title = "zupaTech HQ", PostCode = "SO50 9DT" };
                context.Venues.Add(venue);
                context.SaveChanges();

                // Create 100 seats over 10 rows (A to J)
                var seats = new List<Seat>();
                for (char c = 'A'; c <= 'J'; c++)
                {
                    for (int n = 1; n <= 10; n++)
                    {
                        seats.Add(new Seat() { Row = c.ToString(), Number = n, VenueID = venue.ID });
                    }
                }

                // Save seats to the database
                context.Seats.AddRange(seats);
                context.SaveChanges();
            }
        }
    }
}
