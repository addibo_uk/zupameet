﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ZupaMeet.Models;

namespace ZupaMeet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // Getter for web host configuration
        public IConfiguration Configuration { get; }

        // Services to configure at runtime
        public void ConfigureServices(IServiceCollection services)
        {
            // Ensure we enable MVC functionality
            services.AddMvc().AddJsonOptions(options => { options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore; });

            // Setup DB context (MS SQL Server running locally - in my case,
            // a Docker container running on Mac OS)
            services.AddDbContext<ZupaMeetContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ZupaMeetDB")));
        }

        // Basic configuration setup at runtime
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // In development mode, ensure we return an HTML exception page
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Add default WebAPI route
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Events}/{action=Index}/{id?}");
            });
        }
    }
}
