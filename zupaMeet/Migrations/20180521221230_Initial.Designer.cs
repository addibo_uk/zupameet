﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using ZupaMeet.Models;

namespace ZupaMeet.Migrations
{
    [DbContext(typeof(ZupaMeetContext))]
    [Migration("20180521221230_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ZupaMeet.Models.Event", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<DateTime>("EndDate");

                    b.Property<int?>("MaxSeatsPerBooking");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("Title")
                        .IsRequired();

                    b.Property<int?>("VenueID");

                    b.HasKey("ID");

                    b.HasIndex("VenueID");

                    b.ToTable("Events");
                });

            modelBuilder.Entity("ZupaMeet.Models.Seat", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Number");

                    b.Property<string>("Row")
                        .IsRequired();

                    b.Property<int>("VenueID");

                    b.HasKey("ID");

                    b.HasIndex("VenueID");

                    b.ToTable("Seats");
                });

            modelBuilder.Entity("ZupaMeet.Models.Venue", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("PostCode");

                    b.Property<string>("Title");

                    b.HasKey("ID");

                    b.ToTable("Venues");
                });

            modelBuilder.Entity("ZupaMeet.Models.Event", b =>
                {
                    b.HasOne("ZupaMeet.Models.Venue", "Venue")
                        .WithMany()
                        .HasForeignKey("VenueID");
                });

            modelBuilder.Entity("ZupaMeet.Models.Seat", b =>
                {
                    b.HasOne("ZupaMeet.Models.Venue", "Venue")
                        .WithMany("Seats")
                        .HasForeignKey("VenueID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
