﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using ZupaMeet.Extensions;

namespace ZupaMeet
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Start basic web server to listen for API requests
            BuildWebHost(args).SeedData().Run();
        }

        // Pass "Startup" object to web server instance to setup contexts and routes
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args).UseStartup<Startup>().Build();
    }
}
